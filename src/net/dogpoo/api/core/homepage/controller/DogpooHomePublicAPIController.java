package net.dogpoo.api.core.homepage.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import net.dogpoo.api.core.homepage.service.DogpooHomePublicAPIService;

@RestController
@RequestMapping("/api/homepage")
public class DogpooHomePublicAPIController {
	
	private static final String JSON_UTF8 = "application/json;charset=UTF-8";
	@Autowired DogpooHomePublicAPIService service;
	
	@RequestMapping(value = "/contact/insertContactMessage", method = RequestMethod.POST, produces = JSON_UTF8)
	public HashMap<String, Object> insertContactMessage(@RequestBody HashMap<String, Object> paramMap) {
		return service.insertContactMessage(paramMap);
	}

}
