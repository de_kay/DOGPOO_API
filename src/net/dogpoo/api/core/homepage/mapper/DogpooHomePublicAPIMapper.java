package net.dogpoo.api.core.homepage.mapper;

import net.dogpoo.api.core.homepage.model.DogpooHomeContactMessageModel;

public interface DogpooHomePublicAPIMapper {
	
	public int insertContactMessage(DogpooHomeContactMessageModel model);
	
}
