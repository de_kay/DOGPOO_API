package net.dogpoo.api.common.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public final class JsonToMapUtil {
	
	public static HashMap<String, Object> jsonParamStrToObjectMap(String params) {
		HashMap<String, Object> paramMap = new HashMap<String, Object>();
		try {
			paramMap = new ObjectMapper().readValue(params, new TypeReference<HashMap<String, Object>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paramMap;
	}
	
	public static HashMap<String, String> jsonParamStrToMap(String params) {
		HashMap<String, String> paramMap = new HashMap<String, String>();
		try {
			paramMap = new ObjectMapper().readValue(params, new TypeReference<HashMap<String, String>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paramMap;
	}
	
	public static List<HashMap<String, String>> jsonParamStrToList(String params) {
		List<HashMap<String, String>> paramMap = new ArrayList<HashMap<String, String>>();
		try {
			paramMap = new ObjectMapper().readValue(params, new TypeReference<List<HashMap<String, String>>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return paramMap;
	}
	
	public static boolean isJsonVaild(String responseBody) {
		if(responseBody.length() == 0) {
			return false;
		}
		String firstWord = responseBody.substring(0, 1);
		if("{".equals(firstWord) || "[".equals(firstWord)) {
			return true;
		} else {
			return false;
		}
	}
}