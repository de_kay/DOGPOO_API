package net.dogpoo.api.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import net.dogpoo.api.common.util.auth.JwtAuthenticationEntryPoint;
import net.dogpoo.api.common.util.auth.JwtRequestFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@PropertySource("classpath:/config/properties/application.properties")
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	// 허용할 주소 목록
	private static final String KEY_NM_JWT_PUBLIC_URL = "${jwt.public.url}";
	
	@Autowired UserDetailsService jwtUserDetailsService;
	@Autowired JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
	@Autowired JwtRequestFilter jwtRequestFilter;
	
	@Value(KEY_NM_JWT_PUBLIC_URL)
	private String jwtPublicURL;
	
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedOrigin("http://localhost:3000");
		configuration.addAllowedOrigin("https://gesture.dogpoo.net");
		configuration.addAllowedOrigin("https://convatar.dogpoo.net");
		configuration.addAllowedMethod("*");
		configuration.addAllowedHeader("*");
		configuration.setAllowCredentials(true);
		configuration.setMaxAge(3600L);
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
	
	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		String[] pathArray = jwtPublicURL.split(",");
		httpSecurity.csrf().disable()
				// Authenticate 가 없어도 허용할 주소를 명시한다.
				.authorizeRequests().antMatchers(pathArray).permitAll().
				// 다른 리퀘스트들은 Authenticate 가 필요하다
				anyRequest().authenticated().and().cors().and()
				// ExceptionHandling 시, jwtAuthenticationEntryPoint를 사용합니다.
//				.addFilterBefore(customAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
				// 상태 없는 세션을 사용합니다. 세션은 사용자의 상태를 저장하는 데 사용되지 않습니다.
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		// 모든 요청에 토큰이 유효한지 확인하기 위해, 필터를 추가합니다.
		httpSecurity.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		// 로드할 위치를 알 수 있도록 인증 관리자를 구성합니다
		// 사용자와 자격 증명을 일치시키는 방법
		// BCryptPasswordEncoder를 사용한다
		auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder());
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}
	
//	@Bean  
//    public CustomAuthenticationFilter customAuthenticationFilter() throws Exception {
//		CustomAuthenticationFilter bcsAuthFilter = new CustomAuthenticationFilter();
//		bcsAuthFilter.setAuthenticationManager(authenticationManager());
//		bcsAuthFilter.setPostOnly(true);
//		bcsAuthFilter.setExtraParameter("target");
//		bcsAuthFilter.setUsernameParameter("username");
//		bcsAuthFilter.setPasswordParameter("password");
//		return bcsAuthFilter;
//	}
}